# Requires fakeroot busybox

ROOT_FS ?= rootfs
BUSYBOX ?= $(shell which busybox)
MACHINE ?= $(shell uname -m)
KERNEL ?= /boot/vmlinuz-$(shell uname -r)
INIT ?= /sbin/init
FHS ?= yes

include aeten-make/tree.mk

ifeq (yes,${FHS})
-include fhs.mk
endif
ifneq (,$(wildcard ${OVERLAY}))
-include ${OVERLAY:%.tree=%.mk}
endif

ifneq (,$(wildcard ${BUSYBOX}))
BUSYBOX_DEPS=${BUSYBOX}
endif

${ROOT_FS}: ${FHS_DEPS} ${BUSYBOX_DEPS} ${OVERLAY_DEPS}
	@rm -rf $@
ifdef FHS_RECIPE
	$(info Creating FHS)
	$(call FHS_RECIPE, $@.tmp, .)
endif
ifneq (,$(wildcard ${BUSYBOX}))
	$(info Installing busybox)
	@\mkdir -p $(addprefix $@.tmp/,sbin lib usr/lib)
	@\cp ${BUSYBOX} $@.tmp/sbin/busybox
	@( cd $@.tmp && mklibs -d lib '${BUSYBOX}' > /dev/null )
	$(foreach cmd,$(shell '${BUSYBOX}' --list-full), \
		@\mkdir -p $$(dirname $@.tmp/${cmd}) ${LF} \
		@\ln -s /sbin/busybox $@.tmp/${cmd} ${LF} \
	)
endif
ifdef OVERLAY_RECIPE
	$(info Installing overlay)
	$(call OVERLAY_RECIPE, $@.tmp, $(dir ${OVERLAY}))
endif
	@[ -f $@.tmp${INIT} ] || [ -L $@.tmp${INIT} ] || { echo None init in root FS; false; }
	@ln -s ${INIT} $@.tmp/init
	@mv $@.tmp $@

${ROOT_FS}.gz: ${ROOT_FS}
	$(info Creating compressed RAM FS)
	@( cd $<; find . -print0 | fakeroot cpio --null -o --format=newc | gzip -9 > ../$(notdir $@) )

launch: ${ROOT_FS}.gz
	qemu-system-${MACHINE} -m 64 -kernel '${KERNEL}' -initrd '${ROOT_FS}.gz' -enable-kvm -nographic  -append "console=ttyS0 mem=64M init=${INIT} rootwait quiet"

clean:
	${RM} -r ${ROOT_FS} ${ROOT_FS}.*
ifeq (yes,${FHS})
	${RM} fhs.mk
endif
ifneq (,$(wildcard ${OVERLAY}))
	${RM} ${OVERLAY:%.tree=%.mk}
endif


.PHONY: launch clean all rootfs
